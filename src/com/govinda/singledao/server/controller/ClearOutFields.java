package com.govinda.singledao.server.controller;

import com.govinda.singledao.dboperators.models.entities.Employee;
import com.govinda.singledao.dboperators.models.entities.Entity;
import com.govinda.singledao.dboperators.models.entities.Student;
import com.govinda.singledao.dboperators.models.entities.Teacher;
import com.govinda.singledao.dboperators.session.Session;
import com.govinda.singledao.dboperators.session.SessionManager;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClearOutFields {

    public void clearFields() throws SQLException, ClassNotFoundException {

        List<Entity> entities = getEntities();

        for (Entity entity: entities){
            SessionManager sessionManager = new SessionManager(entity.dbName());

            Session session = sessionManager.getOneTimeSession();

            PreparedStatement preparedStatement = session.prepareStatement("TRUNCATE TABLE "+entity.tableName());

            preparedStatement.executeUpdate();
        }
    }

    private List<Entity> getEntities() throws ClassNotFoundException {
        List<Entity> entities = new ArrayList<>();

        entities.add(Class.forName(Student.class.getName()).getAnnotation(Entity.class));
        entities.add(Class.forName(Employee.class.getName()).getAnnotation(Entity.class));
        entities.add(Class.forName(Teacher.class.getName()).getAnnotation(Entity.class));

        return entities;
    }

}
