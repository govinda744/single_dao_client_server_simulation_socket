package com.govinda.singledao.server.controller;

import com.govinda.singledao.dboperators.dao.SingleDao;
import com.govinda.singledao.dboperators.dao.SingleDaoImpl;
import com.govinda.singledao.dboperators.models.EntityDeserialization;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class Controller<T> {
    final private SingleDao<T> dao;
    private final HashMap<String, String> executionParameters;
    private final EntityDeserialization<T> deserialization;
    private String result ;

    public Controller(Class<T> callingClass, HashMap<String,String> parameters) {
        executionParameters = parameters;
        deserialization = new EntityDeserialization<>(callingClass);
        this.dao = new SingleDaoImpl<>(callingClass);

    }

    public void execute() {
        String choice = executionParameters.get("operation");

        switch (choice) {
            case "add":
                int addId = Integer.parseInt(executionParameters.get("id"));
                if(!entryAvailableAt(addId))
                    result = add();
                else
                    System.out.println("Entry already at "+addId);
                break;
            case "delete":
                int deleteAtId = Integer.parseInt(executionParameters.get("id"));
                if(!isRelationEmpty() && entryAvailableAt(deleteAtId)){
                    result = delete(deleteAtId);
                } else {
                    result = ("Entry not available for deleting at "+ deleteAtId+" for "+executionParameters.get("models"));
                }
                break;
            case "edit":
                int editAtId = Integer.parseInt(executionParameters.get("id"));
                if(!isRelationEmpty() && entryAvailableAt(editAtId)) {
                    result = edit(editAtId);
                } else {
                    result = ("Entry not available for editing at "+ editAtId+" for "+executionParameters.get("models"));
                }
                break;
            case "viewall":
                result = viewAll();
                break;
            default :
                break;
        }
    }

    public String getResponse() {
        return result;
    }

    private String add() {
        HashMap<String, String> values = filterValues(executionParameters);
        int id = 0;
        try {
            id = dao.merge(deserialization.toEntity(values));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Added to "+executionParameters.get("models")+" at id "+ id;
    }

    private String delete(int id) {
        try {
            dao.delete(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Deleted from "+executionParameters.get("models")+" at id "+id;
    }

    private String viewAll() {
        List<T> resultList = null;
        try {
            resultList = dao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (T result: resultList) {
            System.out.println(result.toString());
        }
        return "viewing "+executionParameters.get("models")+"s";
    }

    private String edit(int id) {
        HashMap<String, String> values = filterValues(executionParameters);
        try {
            dao.update(deserialization.toEntity(values));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "edited values of "+executionParameters.get("models")+" at id "+id;
    }

    private HashMap<String, String> filterValues(HashMap<String, String> parameters) {
        HashMap<String, String> returnValues = new HashMap<>();
        returnValues.put("id", parameters.get("id"));
        returnValues.put("name", parameters.get("name"));
        returnValues.put("address", parameters.get("address"));
        returnValues.put("contact", parameters.get("contact"));
        return returnValues;
    }

    private boolean isRelationEmpty() {
        try {
            return dao.getAll() == null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    private boolean entryAvailableAt(int id) {
        try {
            return dao.get(id) != null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
