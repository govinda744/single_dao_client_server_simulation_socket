package com.govinda.singledao.server;

import com.govinda.singledao.server.controller.Controller;
import com.govinda.singledao.dboperators.models.entities.Employee;
import com.govinda.singledao.dboperators.models.entities.Student;
import com.govinda.singledao.dboperators.models.entities.Teacher;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class Server implements Runnable{

    private ServerSocket serverSocket;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    @Override
    public void run() {
        while (true) {
            try {
                new Thread(new ClientHandler(serverSocket.accept())).start();
            } catch (IOException|ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    class ClientHandler implements Runnable {

        private final Socket clientSocket;
        private final HashMap<String, String> requestByClient;
        private final PrintWriter outToClient;
        private final ObjectInputStream inFromClient;

        ClientHandler(Socket clientSocket) throws IOException, ClassNotFoundException {
            this.clientSocket = clientSocket;
            outToClient = new PrintWriter(clientSocket.getOutputStream(), true);
            inFromClient = new ObjectInputStream(clientSocket.getInputStream());
            requestByClient = (HashMap<String, String>) inFromClient.readObject();
        }

        @Override
        public void run() {
            String choice = requestByClient.get("models");
            switch (choice) {

                case "Student":
                    Controller<Student> studentController = new Controller<>(Student.class,requestByClient);
                    studentController.execute();
                    sendToClient(studentController.getResponse());
                    break;
                case "Teacher":
                    Controller<Teacher> teacherController = new Controller<>(Teacher.class,requestByClient);
                    teacherController.execute();
                    sendToClient(teacherController.getResponse());
                    break;
                case "Employee":
                    Controller<Employee> employeeController = new Controller<>(Employee.class,requestByClient);
                    employeeController.execute();
                    sendToClient(employeeController.getResponse());
                    break;
                default:
                    break;
            }
            closeUsedResources();
        }

        private void sendToClient(String result) {
            outToClient.println(result);
        }

        private void closeUsedResources() {
            try {
                inFromClient.close();
                outToClient.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
