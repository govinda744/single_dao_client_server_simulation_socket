package com.govinda.singledao;

import com.govinda.singledao.client.Clients;
import com.govinda.singledao.server.Server;
import com.govinda.singledao.server.controller.ClearOutFields;
import com.govinda.singledao.dboperators.models.databases.EmployeeDb;
import com.govinda.singledao.dboperators.models.databases.SchoolDb;
import com.govinda.singledao.dboperators.session.ConnectionPoolImpl;

import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        ConnectionPoolImpl.create(SchoolDb.NAME,SchoolDb.USER,SchoolDb.PASSWORD);
        ConnectionPoolImpl.create(EmployeeDb.NAME,EmployeeDb.USER,EmployeeDb.PASSWORD);

        new ClearOutFields().clearFields();

        Thread server = new Thread(new Server(32145));
        Thread clients = new Thread(new Clients());

        clients.start();
        server.start();

    }
}
