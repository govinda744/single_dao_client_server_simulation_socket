package com.govinda.singledao.dboperators.session;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionPool {

    Connection getConnection(String dbName) throws SQLException;
    boolean releaseConnection(String dbName, Connection connection);

}
