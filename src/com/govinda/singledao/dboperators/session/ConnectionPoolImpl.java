package com.govinda.singledao.dboperators.session;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ConnectionPoolImpl implements ConnectionPool {

    private static final int INITIAL_POOL_SIZE = 10;

    private static HashMap<String,List<Connection>> connectionPool = new HashMap<>();

    public ConnectionPoolImpl(){}

    private ConnectionPoolImpl(String dbName, List<Connection> pool){
        this.connectionPool.put(dbName,pool);
    }

    public static ConnectionPoolImpl create(String dbName, String user, String password) throws SQLException {

        if(!connectionPool.containsKey(dbName)) {
            List<Connection> pool = new ArrayList<>(INITIAL_POOL_SIZE);
            for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
                pool.add(createConnection(dbName, user, password));
            }
            return new ConnectionPoolImpl(dbName, pool);
        }
        else {
            return new ConnectionPoolImpl();
        }
    }

    private static Connection createConnection(String dbName,String user,String password) throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost/"+dbName+"?useSSL=true", user, password);
    }

    @Override
    public Connection getConnection(String dbName) {
        return connectionPool.get(dbName).remove(connectionPool.get(dbName).size() - 1);
    }

    @Override
    public boolean releaseConnection(String dbName, Connection connection) {
        return connectionPool.get(dbName).add(connection);
    }
}