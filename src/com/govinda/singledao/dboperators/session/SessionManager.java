package com.govinda.singledao.dboperators.session;

public class SessionManager extends Session{

    public SessionManager(String dbName) {
        super(dbName);
    }

    public Session getOneTimeSession() {
        return getSession();
    }
}
