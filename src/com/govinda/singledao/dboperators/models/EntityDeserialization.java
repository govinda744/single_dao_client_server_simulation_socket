package com.govinda.singledao.dboperators.models;

import com.govinda.singledao.dboperators.models.entities.ColumnName;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class EntityDeserialization<T> {

    private Class<T> callingClass;

    public EntityDeserialization(Class<T> callingClass) {
        this.callingClass = callingClass;
    }

    //deserialization
    public T toEntity(ResultSet resultSet) {

        T entity = getEntity(callingClass);

        Method[] methods = entity.getClass().getMethods();

        for(Method method:methods){
            ColumnName setValueOf = method.getAnnotation(ColumnName.class);
            if(method.getName().toUpperCase().contains("SET") && setValueOf != null ){
                try {
                    method.invoke(entity, setValueOf.columnName().equals("id") ? resultSet.getInt(setValueOf.columnName()) : resultSet.getString(setValueOf.columnName()));
                } catch (IllegalAccessException|InvocationTargetException|SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return entity;
    }

    public T toEntity(HashMap<String, String> values) {

        T entity = getEntity(callingClass);

        Method[] methods = entity.getClass().getMethods();

        for(Method method:methods){
            ColumnName setValueOf = method.getAnnotation(ColumnName.class);
            if(method.getName().toUpperCase().contains("SET") && setValueOf != null){
                try {
                    method.invoke(entity, setValueOf.columnName().equals("id")?Integer.parseInt(values.get(setValueOf.columnName())):values.get(setValueOf.columnName()));
                } catch (IllegalAccessException|InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return entity;
    }

    private T getEntity(Class<T> callingClass) {
        T entity = null;
        try {
            entity = (T) Class.forName(callingClass.getName()).newInstance();
        } catch (InstantiationException|IllegalAccessException|ClassNotFoundException e) {
            e.printStackTrace();
        }

        return entity;
    }
}
