package com.govinda.singledao.dboperators.models;

import com.govinda.singledao.dboperators.models.entities.ColumnName;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class EntitySerialization<T> {

    //Serialization
    public HashMap<String, String> toHashMap(T object) {

        Method[] methods = object.getClass().getMethods();

        HashMap<String,String> columnsWithValues = new HashMap<>();

        for(Method method:methods){
            ColumnName getColumn = method.getAnnotation(ColumnName.class);
            if(method.getName().toUpperCase().contains("GET") && getColumn != null){
                try {
                    columnsWithValues.put(getColumn.columnName(),String.valueOf(method.invoke(object)));
                } catch (IllegalAccessException|InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        return columnsWithValues;
    }
}