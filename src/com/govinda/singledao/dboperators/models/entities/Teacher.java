package com.govinda.singledao.dboperators.models.entities;

import com.govinda.singledao.dboperators.models.databases.SchoolDb;

@Entity(dbName = SchoolDb.NAME, tableName = Teacher.TABLE_NAME)
public class Teacher {

    public static final String TABLE_NAME = "teacher_info";

    private int id;
    private String name;
    private String address;
    private String contact;

    @ColumnName(columnName = "id")
    public int getId() {
        return id;
    }

    @ColumnName(columnName = "name")
    public String getName() {
        return name;
    }

    @ColumnName(columnName = "address")
    public String getAddress() {
        return address;
    }

    @ColumnName(columnName = "contact")
    public String getContact() {
        return contact;
    }

    @ColumnName(columnName = "id")
    public void setId(int id) {
        this.id = id;
    }

    @ColumnName(columnName = "name")
    public void setName(String name) {
        this.name = name;
    }

    @ColumnName(columnName = "address")
    public void setAddress(String address) {
        this.address = address;
    }

    @ColumnName(columnName = "contact")
    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", contact='" + contact + '\'' +
                '}';
    }
}