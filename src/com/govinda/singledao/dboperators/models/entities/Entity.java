package com.govinda.singledao.dboperators.models.entities;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)

public @interface Entity {
    String dbName() default "";
    String tableName() default "";
}
