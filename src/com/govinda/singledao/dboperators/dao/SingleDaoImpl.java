package com.govinda.singledao.dboperators.dao;

import com.govinda.singledao.dboperators.models.EntityDeserialization;
import com.govinda.singledao.dboperators.models.EntitySerialization;
import com.govinda.singledao.dboperators.models.entities.Entity;
import com.govinda.singledao.dboperators.session.Session;
import com.govinda.singledao.dboperators.session.SessionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class SingleDaoImpl<T> implements SingleDao<T> {

    SessionManager sessionManager;
    private String tableName;
    private EntitySerialization<T> entitySerialization;
    private EntityDeserialization<T> entityDeserialization;

    public SingleDaoImpl(Class<T> callingClass) {

        entitySerialization = new EntitySerialization<>();
        entityDeserialization = new EntityDeserialization<>(callingClass);
        Entity entity = null;
        try {
            entity = Class.forName(callingClass.getName()).getAnnotation(Entity.class);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        sessionManager = new SessionManager(entity.dbName());
        tableName = entity.tableName();
    }

    @Override
    public int add(T entity) throws SQLException {

        HashMap<String, String> columnsNameAndValue = getColumnsNamesAndValues(entity);

        String sqlQuery = "INSERT INTO " + tableName + " " +
                "(" + columnsNameAndValue.get("names") + ") " +
                "VALUES (" + columnsNameAndValue.get("values") + ");";

        String[] returnId = {"id"};

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery,returnId);
        setParameterValues(preparedStatement,entity);
        ResultSet resultSet = preparedStatement.executeUpdate() != 0 ? preparedStatement.getGeneratedKeys() : null;
        session.closeSession();

        if(resultSet.next()){
            return resultSet.getInt(1);
        } else {
            return 0;
        }
    }

    @Override
    public boolean delete(int id) throws SQLException {

        String sqlQuery = "DELETE FROM " + tableName + " " +
                "WHERE id = ?";

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery);
        preparedStatement.setInt(1, id);
        boolean result = preparedStatement.executeUpdate() != 0;
        session.closeSession();

        return result;
    }

    @Override
    public List<T> getAll() throws SQLException {
        List<T> resultList = new ArrayList<>();

        String sqlQuery = "SELECT * " +
                "FROM " + tableName;

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            resultList.add(entityDeserialization.toEntity(resultSet));
        }
        session.closeSession();
        return resultList;
    }

    @Override
    public int update(T entity) throws SQLException {

        String sqlQuery = "UPDATE " + tableName + " " +
                "SET " + getUpdatingColumnsWithParameters(entity) + " " +
                "WHERE id = ?";

        int id = Integer.parseInt(entitySerialization.toHashMap(entity).get("id"));

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery);

        int  parameterIndex = setParameterValues(preparedStatement,entity);
        preparedStatement.setInt(parameterIndex, id);

        int result = preparedStatement.executeUpdate();
        session.closeSession();

        return result != 0 ? id : 0;
    }

    @Override
    public T get(int id) throws SQLException {
        T returningEntity = null;
        String sqlQuery = "SELECT * " +
                "FROM " + tableName + " " +
                "WHERE id = ?";

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            returningEntity = entityDeserialization.toEntity(resultSet);
        }
        session.closeSession();

        return returningEntity;
    }

    @Override
    public int merge(T entity) throws  SQLException {

        int id = Integer.parseInt(entitySerialization.toHashMap(entity).get("id"));

        String query = "SELECT  * " +
                "FROM " + tableName + " " +
                "WHERE id = " + id;

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        session.closeSession();

        if (resultSet.next()) {
            int result = update(entity);
            return result;
        } else {
            int result = add(entity);
            return result;
        }
    }

    private int setParameterValues(PreparedStatement preparedStatement, T entity) throws SQLException {
        int parameterIndex = 1;
        for (Map.Entry<String, String> entry : entitySerialization.toHashMap(entity).entrySet()) {
            if (!entry.getKey().equals("id") && !entry.getValue().isEmpty()) {
                preparedStatement.setString(parameterIndex, entry.getValue());
                parameterIndex++;
            }
        }
        return parameterIndex;
    }

    private HashMap<String, String> getColumnsNamesAndValues(T entity) {
        StringBuilder columnsName = new StringBuilder();
        StringBuilder columnsValue = new StringBuilder();

        HashMap<String, String> namesWithValues = entitySerialization.toHashMap(entity);

        for (Map.Entry<String, String> entry : namesWithValues.entrySet()) {
            if (!entry.getKey().equals("id") && !entry.getValue().isEmpty()) {
                columnsName.append(entry.getKey()).append(",");
                columnsValue.append("?,");
            }
        }
        columnsName.deleteCharAt(columnsName.toString().length() - 1);
        columnsValue.deleteCharAt(columnsValue.toString().length() - 1);

        namesWithValues.clear();
        namesWithValues.put("names",String.valueOf(columnsName));
        namesWithValues.put("values", String.valueOf(columnsValue));

        return namesWithValues;
    }

    private String getUpdatingColumnsWithParameters(T entity) {
        StringBuilder columns = new StringBuilder();
        HashMap<String, String> entityColumns = entitySerialization.toHashMap(entity);

        Set<Map.Entry<String, String>> entrySet = entityColumns.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            if (!entry.getKey().equals("id") && !entry.getValue().isEmpty()) {
                columns.append(entry.getKey()).append("=?,");
            }
        }

        columns = columns.deleteCharAt(columns.toString().length() - 1);

        return String.valueOf(columns);
    }

}
