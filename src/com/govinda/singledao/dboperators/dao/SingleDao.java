package com.govinda.singledao.dboperators.dao;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface SingleDao<T> {

    int add(T dbEntity) throws SQLException;
    boolean delete(int id) throws SQLException;
    List<T> getAll() throws SQLException;
    int update(T entity) throws SQLException;
    T get(int id) throws SQLException;
    int merge(T dbEntity) throws  SQLException;

}
